﻿
Imports System.IO
Imports Microsoft.Win32

Class MainWindow

    Private tokenList As New ArrayList
    Private classList As New ArrayList
    Private errorList As New ArrayList
    Private associationList As New ArrayList
    Private route As String = ""

    Private Sub Click_analyze(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim instructions As String()
        instructions = Split(txtTextArea.Text, vbCrLf)
        Dim readerString As New ReaderString(instructions)
        tokenList = readerString.lineSeparator()
        errorList = readerString.errorsList
        Dim lexical = readerString.lexical
        Dim syntacticAnalisis As New Syntactic_Analysis(errorList)
        tokenList.Add(New Token("##", "Aceptacion", 0, 0))
        syntacticAnalisis.Parser(tokenList)
        errorList = syntacticAnalisis.errorList
        tokenList.RemoveAt(tokenList.Count - 1)
        Dim lexicalSystem As New LexicalSystem(tokenList)
        classList = lexicalSystem.SaveClass()
        associationList = lexicalSystem.SaveAssociation()
        MsgBox("Texto analizado correctamente")
    End Sub

    Private Sub Click_open(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim fd As OpenFileDialog = New OpenFileDialog()
        fd.Filter = "Archivos|*.lfp"
        fd.ShowDialog()
        route = fd.FileName
        Try
            Dim strt As New System.IO.StreamReader(route)
            Dim text = strt.ReadToEnd
            txtTextArea.Text = text
            strt.Close()
        Catch ex As Exception

        End Try


    End Sub

    Private Sub Click_save(ByVal sender As Object, ByVal e As System.EventArgs)
        If route.Equals("") Then
            Dim fd As SaveFileDialog = New SaveFileDialog()
            fd.Filter = "Archivos|*.lfp"
            fd.ShowDialog()
            Try
                My.Computer.FileSystem.WriteAllText(fd.FileName, txtTextArea.Text, True)
            Catch ex As Exception

            End Try
        Else
            Dim save As New StreamWriter(route)
            Try
                save.WriteLine(txtTextArea.Text)
                save.Close()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub Click_saveAs(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim fd As SaveFileDialog = New SaveFileDialog()
        fd.Filter = "Archivos|*.lfp"
        fd.ShowDialog()
        Try
            My.Computer.FileSystem.WriteAllText(fd.FileName, txtTextArea.Text, True)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Click_report(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strBody As String
        Dim token As Token
        If errorList.Count > 0 Then
            For i As Integer = 0 To errorList.Count - 1 Step 1
                token = errorList(i)
                strBody = strBody & "<tr>" & "<td>" & i + 1 & "</td>" & "<td>" & token.typeErrors & "</td>" & "<td>" & token.Lexemes & "</td>" & "<td>" & token.columns & "</td>" & "<td>" & token.lines & "</td>" & "<td>" & token.descritptionErrors & "</td>" & "</tr>"
            Next
            Dim htmlFilE As New StreamWriter("tabla.html")
            Try
                htmlFilE.WriteLine("
<table>
  <thead>
    <tr>
      <th>No.</th>
        <th>Tipo</th>
      <th>Error</th>
      <th>Columna</th>
      <th>Linea</th>
<th>Descripcion</th>
    </tr>
  </thead>
  <tbody>" & strBody & "
    
  </tbody>
</table>
<!-- Codes by Quackit.com -->

")
                htmlFilE.Close()
            Catch ex As Exception

            End Try
            System.Diagnostics.Process.Start("tabla.html")
        ElseIf tokenList.Count > 0 Then
            For i As Integer = 0 To tokenList.Count - 1 Step 1
                token = tokenList(i)
                strBody = strBody & "<tr>" & "<td>" & i + 1 & "</td>" & "<td>" & token.Lexemes & "</td>" & "<td>" & token.types & "</td>" & "<td>" & token.columns & "</td>" & "<td>" & token.lines & "</td>" & "</tr>"
            Next
            Dim htmlFilE As New StreamWriter("tabla.html")
            Try
                htmlFilE.WriteLine("
<table>
  <thead>
    <tr>
      <th>No.</th>
      <th>Lexema</th>
      <th>Tipo</th>
      <th>Columna</th>
      <th>Linea</th>
    </tr>
  </thead>
  <tbody>" & strBody & "
    
  </tbody>
</table>
<!-- Codes by Quackit.com -->

")
                htmlFilE.Close()
            Catch ex As Exception

            End Try
            System.Diagnostics.Process.Start("tabla.html")
        Else
            MsgBox("Error no hay ningun texto analaizado")
        End If



    End Sub

    Private Sub Click_diagram(ByVal sender As Object, ByVal e As System.EventArgs)
        If errorList.Count > 0 Then
            MsgBox("Hay errores, no se puede diagramar")
            Exit Sub
        End If
        Dim token As Token
        Dim sw As New StreamWriter("diagrama.txt")
        Dim strContruct As String
        Dim strAttribute As String
        Dim strMethod As String
        Dim atrList As Attribute
        Dim mthList As Method
        For i As Integer = 0 To associationList.Count - 1 Step 1
            Dim Association As Association = associationList(i)
            Dim exist1 As Boolean
            Dim exist2 As Boolean
            For y As Integer = 0 To classList.Count - 1 Step 1
                Dim classs As Classs = classList(y)
                If Association.names1.Equals(classs.names) Then
                    exist1 = True
                    If classs.attributes IsNot Nothing Then
                        For k As Integer = 0 To classs.attributes.Count - 1 Step 1
                            atrList = classs.attributes(k)
                            If atrList.types <> Nothing Then
                                strAttribute = strAttribute & atrList.visibilities & " " & atrList.names & ":" & atrList.types & "\n"
                            Else
                                strAttribute = strAttribute & atrList.visibilities & " " & atrList.names & "\n"
                            End If
                        Next
                    End If
                    For n As Integer = 0 To classs.methods.Count - 1 Step 1
                        mthList = classs.methods(n)
                        If mthList.types <> Nothing Then
                            strMethod = strMethod & mthList.visibilities & " " & mthList.names & "()" & ":" & mthList.types & "\n"
                        Else
                            strMethod = strMethod & mthList.visibilities & " " & mthList.names & "()" & "\n"
                        End If
                    Next
                    strContruct = strContruct & classs.names & " " & "[label = """ & classs.names & "|" & " " & strAttribute & " " & "|" & " " & strMethod & " ""];" & vbCrLf
                ElseIf Association.names2.Equals(classs.names) Then
                    exist2 = True
                    If classs.attributes IsNot Nothing Then
                        For k As Integer = 0 To classs.attributes.Count - 1 Step 1
                            atrList = classs.attributes(k)
                            If atrList.types <> Nothing Then
                                strAttribute = strAttribute & atrList.visibilities & " " & atrList.names & ":" & atrList.types & "\n"
                            Else
                                strAttribute = strAttribute & atrList.visibilities & " " & atrList.names & "\n"
                            End If
                        Next
                    End If
                    For n As Integer = 0 To classs.methods.Count - 1 Step 1
                        mthList = classs.methods(n)
                        If mthList.types <> Nothing Then
                            strMethod = strMethod & mthList.visibilities & " " & mthList.names & "()" & ":" & mthList.types & "\n"
                        Else
                            strMethod = strMethod & mthList.visibilities & " " & mthList.names & "()" & "\n"
                        End If
                    Next
                    strContruct = strContruct & classs.names & " " & "[label = """ & classs.names & "|" & " " & strAttribute & " " & "|" & " " & strMethod & " ""];" & vbCrLf
                End If
            Next
            If exist1 = True And exist2 = True Then
                If Association.typeAssociations.Equals("agregacion") Then
                    strContruct = strContruct & Association.names1 & " " & "->" & " " & Association.names2 & " " & "[arrowhead=""odiamond""]" & vbCrLf
                ElseIf Association.typeAssociations.Equals("composicion") Then
                    strContruct = strContruct & Association.names1 & " " & "->" & " " & Association.names2 & " " & "[arrowhead=""diamond""]" & vbCrLf
                ElseIf Association.typeAssociations.Equals("herencia") Then
                    strContruct = strContruct & Association.names1 & " " & "->" & " " & Association.names2 & " " & "[arrowhead=""onormal""]" & vbCrLf
                ElseIf Association.typeAssociations.Equals("asociacionsimple") Then
                    strContruct = strContruct & Association.names1 & " " & "->" & " " & Association.names2 & " " & "[arrowhead=""none""]" & vbCrLf
                End If
            End If
        Next
        sw.WriteLine("digraph G {" & vbCrLf &
                     "rankdir=LR;" & vbCrLf &
                     "size=""5,5""" & vbCrLf &
                     "node [shape=record, style=filled, fillcolor=gray95];" & vbCrLf & strContruct & vbCrLf & "}")
        sw.Close()
        Dim prog As VariantType
        prog = Interaction.Shell("dot.exe -Tsvg diagrama.txt -o diagrama.svg", 0)
    End Sub

    Private Sub Click_exit(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub Click_info(ByVal sender As Object, ByVal e As System.EventArgs)
        MsgBox("Autor: Eddie Augusto Alvarez Salzar" & Chr(13) & "Carne: 201700326")
    End Sub

    Private Sub Click_manual(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Diagnostics.Process.Start("Manual.pdf")
    End Sub

End Class
