﻿Public Class LexicalSystem

    Private tokenList As ArrayList
    Private classList As New ArrayList
    Private associationList As New ArrayList

    Public Sub New(tokenList As ArrayList)
        Me.tokenList = tokenList
    End Sub

    Public Function SaveClass() As ArrayList
        Dim lexeme As String
        Dim strAux As String
        For i As Integer = 0 To tokenList.Count - 1 Step 1
            Dim token As Token
            token = tokenList(i)
            lexeme = token.Lexemes
            Select Case lexeme
                Case "clase"
                    Dim classs As New Classs("", Nothing, Nothing, Nothing)
                    For n As Integer = i + 4 To tokenList.Count - 1 Step 1
                        token = tokenList(n)
                        strAux = token.Lexemes
                        If strAux.Equals("clase") Then
                            Exit For
                        End If
                        Select Case strAux
                            Case "nombre"
                                token = tokenList(n + 3)
                                Dim name As String = token.Lexemes
                                classs.names = name
                            Case "atributos"
                                Dim attributeList As New ArrayList
                                For y As Integer = n + 3 To tokenList.Count - 1 Step 1
                                    token = tokenList(y)
                                    If token.Lexemes.Equals("}") Then
                                        Exit For
                                    End If
                                    y = y + 1
                                    token = tokenList(y)
                                    Dim visibility As String = token.Lexemes
                                    token = tokenList(y + 2)
                                    Dim id As String = token.Lexemes
                                    token = tokenList(y + 4)
                                    If token.types.Equals("Identificador") Then
                                        Dim type As String = token.Lexemes
                                        Dim attribute As New Attribute(visibility, id, type)
                                        attributeList.Add(attribute)
                                        y = y + 5
                                    Else
                                        Dim attribute As New Attribute(visibility, id, Nothing)
                                        attributeList.Add(attribute)
                                        y = y + 3
                                    End If
                                Next
                                classs.attributes = attributeList
                            Case "metodos"
                                Dim methodList As New ArrayList
                                For y As Integer = n + 3 To tokenList.Count - 1 Step 1
                                    token = tokenList(y)
                                    If token.Lexemes.Equals("}") Then
                                        Exit For
                                    End If
                                    y = y + 1
                                    token = tokenList(y)
                                    Dim visibility As String = token.Lexemes
                                    token = tokenList(y + 2)
                                    Dim id As String = token.Lexemes
                                    token = tokenList(y + 4)
                                    If token.types.Equals("Identificador") Then
                                        Dim type As String = token.Lexemes
                                        Dim method As New Method(visibility, id, type)
                                        methodList.Add(method)
                                        y = y + 5
                                    Else
                                        Dim method As New Method(visibility, id, Nothing)
                                        methodList.Add(method)
                                        y = y + 3
                                    End If
                                Next
                                classs.methods = methodList
                        End Select
                    Next
                    classList.Add(classs)
            End Select
        Next
        Return classList
    End Function

    Public Function SaveAssociation() As ArrayList
        Dim lexeme As String
        For i As Integer = 0 To tokenList.Count - 1 Step 1
            Dim token As Token
            token = tokenList(i)
            lexeme = token.Lexemes
            Select Case lexeme
                Case "asociacion"
                    Dim association As New Association("", "", "")
                    For n As Integer = i + 3 To tokenList.Count - 1 Step 1
                        token = tokenList(n)
                        If token.Lexemes.Equals("}") Then
                            Exit For
                        End If
                        Dim name1 As String = token.Lexemes
                        token = tokenList(n + 2)
                        Dim associationSTR As String = token.Lexemes
                        token = tokenList(n + 4)
                        Dim name2 As String = token.Lexemes
                        Dim associations As New Association(name1.ToLower, associationSTR.ToLower, name2.ToLower)
                        associationList.Add(associations)
                        n = n + 5
                    Next
            End Select
        Next
        Return associationList
    End Function

End Class
