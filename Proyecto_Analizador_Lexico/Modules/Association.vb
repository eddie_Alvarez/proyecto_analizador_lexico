﻿Public Class Association

    Private name1 As String
    Private typeAssociation As String
    Private name2 As String

    Public Property names1 As String
        Get
            Return Me.name1
        End Get
        Set(value As String)
            Me.name1 = value
        End Set
    End Property

    Public Property names2 As String
        Get
            Return Me.name2
        End Get
        Set(value As String)
            Me.name2 = value
        End Set
    End Property

    Public Property typeAssociations As String
        Get
            Return Me.typeAssociation
        End Get
        Set(value As String)
            Me.typeAssociation = value
        End Set
    End Property

    Public Sub New(name1 As String, typeAssociation As String, name2 As String)
        Me.name1 = name1
        Me.typeAssociation = typeAssociation
        Me.name2 = name2
    End Sub

End Class
