﻿Public Class Commentary

    Private name As String
    Private text As String

    Public Property names As String
        Get
            Return Me.name
        End Get
        Set(value As String)
            Me.name = value
        End Set
    End Property

    Public Property texts As String
        Get
            Return Me.text
        End Get
        Set(value As String)
            Me.text = value
        End Set
    End Property

    Public Sub New(name As String, text As String)
        Me.name = name
        Me.text = text
    End Sub

End Class
