﻿Public Class Method

#Region "Declarations"
    Private visibility As String
    Private name As String
    Private type As String
#End Region

#Region "Properties"
    Public Property visibilities As String
        Get
            Return Me.visibility
        End Get
        Set(value As String)
            Me.visibility = value
        End Set
    End Property

    Public Property names As String
        Get
            Return Me.name
        End Get
        Set(value As String)
            Me.name = value
        End Set
    End Property

    Public Property types As String
        Get
            Return Me.type
        End Get
        Set(value As String)
            Me.type = value
        End Set
    End Property

#End Region

#Region "Constructors"
    Public Sub New(visibility As String, name As String, type As String)
        Me.visibility = visibility
        Me.name = name
        Me.type = type
    End Sub
#End Region

End Class
