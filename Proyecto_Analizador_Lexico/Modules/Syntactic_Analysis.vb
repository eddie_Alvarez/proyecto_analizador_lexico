﻿Public Class Syntactic_Analysis

    Dim numPreanalysis As Integer
    Dim preanalysis As Token
    Dim tokenList As ArrayList
    Public errorList As ArrayList

    Public Sub New(errorList As ArrayList)
        Me.errorList = errorList
    End Sub

    Public Sub Parser(tokenList As ArrayList)
        Me.tokenList = tokenList
        preanalysis = Me.tokenList(0)
        numPreanalysis = 0
        S()
    End Sub

    Private Sub S()
        If preanalysis.Lexemes.Equals("[") Then
            match("Corchete Abierto")
            A()
        Else
            match("Aceptacion")
        End If
    End Sub

    Private Sub A()
        If preanalysis.Lexemes = "clase" Then
            match2("clase")
            match("Corchete Cerrado")
            match("Llave Abierta")
            B()
            A()
        ElseIf preanalysis.Lexemes = "comentario" Then
            match2("comentario")
            match("Corchete Cerrado")
            match("Llave Abierta")
            H()
            A()
        ElseIf preanalysis.Lexemes = "asociacion" Then
            match2("asociacion")
            match("Corchete Cerrado")
            match("Llave Abierta")
            J()
            A()
        End If
    End Sub

    Private Sub B()
        If preanalysis.Lexemes.Equals("[") Then
            match("Corchete Abierto")
            C()
            B()
        Else
            match("Llave Cerrada")
        End If
    End Sub

    Private Sub C()
        If preanalysis.Lexemes = "nombre" Then
            match2("nombre")
            match("Corchete Cerrado")
            match("Signo Igual")
            match("Identificador")
            match("Punto y coma")
        ElseIf preanalysis.Lexemes = "color" Then
            match2("color")
            match("Corchete Cerrado")
            match("Signo Igual")
            match("Numero")
            match("Coma")
            match("Numero")
            match("Coma")
            match("Numero")
            match("Punto y coma")
        ElseIf preanalysis.Lexemes = "atributos" Then
            match2("atributos")
            match("Corchete Cerrado")
            match("Llave Abierta")
            D()
        ElseIf preanalysis.Lexemes = "metodos" Then
            match2("metodos")
            match("Corchete Cerrado")
            match("Llave Abierta")
            D()
        End If
    End Sub

    Private Sub D()
        If preanalysis.Lexemes.Equals("(") Then
            match("Parentesis Abierto")
            F()
            match("Parentesis Cerrado")
            match("Identificador")
            G()
            D()
        Else
            match("Llave Cerrada")
        End If
    End Sub

    Private Sub F()
        If preanalysis.types = "Visibilidad" Then
            match("Visibilidad")
        End If
    End Sub

    Private Sub G()
        If preanalysis.types = "Dos puntos" Then
            match("Dos puntos")
            match("Identificador")
            match("Punto y coma")
        Else
            match("Punto y coma")
        End If
    End Sub

    Private Sub H()
        If preanalysis.Lexemes.Equals("{") Then
            match("Llave Abierta")
            match("Corchete Abierto")
            I()
        Else
            match("Llave Cerrada")
        End If
    End Sub

    Private Sub I()
        If preanalysis.Lexemes = "nombre" Then
            match2("nombre")
            match("Corchete Cerrado")
            match("Signo Igual")
            match("Identificador")
            match("Punto y coma")
        Else
            match2("texto")
            match("Corchete Cerrado")
            match("Signo Igual")
            match("Comillas")
            match("Comentario")
            match("Comillas")
            match("Punto y coma")
        End If
    End Sub

    Private Sub J()
        match("Llave Abierta")
        K()
        match("Llave Cerrada")
    End Sub

    Private Sub K()
        If preanalysis.types.Equals("Identificador") Then
            match("Identificador")
            match("Dos puntos")
            L()
            K()
        End If
    End Sub

    Private Sub L()
        If preanalysis.types = "Tipo de Asociacion" Then
            match("Tipo de Asociacion")
            match("Dos puntos")
            match("Identificador")
            match("Punto y coma")
        Else
            match("Identificador")
            match("Punto y coma")
        End If
    End Sub


    Private Sub match(type As String)
        Try
            If Not type = preanalysis.types Then
                Dim token As Token = tokenList(numPreanalysis)
                Dim tokenError As New Token(token.Lexemes, token.columns, token.lines, "Se esperaba " + type, "Sintactico", type)
                errorList.Add(tokenError)
            Else
                numPreanalysis = numPreanalysis + 1
                preanalysis = tokenList(numPreanalysis)
            End If
        Catch e As Exception
            Console.WriteLine("Analizado correctamente")
        End Try
    End Sub

    Private Sub match2(lexeme As String)
        Try
            If Not lexeme = preanalysis.Lexemes Then
                Dim token As Token = tokenList(numPreanalysis)
                Dim tokenError As New Token(token.Lexemes, token.columns, token.lines, "Se esperaba " + lexeme, "Sintactico", token.types)
                errorList.Add(tokenError)
            Else
                numPreanalysis = numPreanalysis + 1
                preanalysis = tokenList(numPreanalysis)
            End If
        Catch e As Exception
            Console.Write("Analizado sintacticamente")
        End Try
    End Sub

End Class
