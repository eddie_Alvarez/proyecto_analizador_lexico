﻿Public Class ReaderString

    Private tokenList As New ArrayList()
    Public lexical
    Private instruccions As String()
    Private errorList As New ArrayList()

    Public Property errorsList As ArrayList
        Get
            Return Me.errorList
        End Get
        Set(value As ArrayList)
            Me.errorList = value
        End Set
    End Property

    Public Sub New(instruccions As String())

        Me.instruccions = instruccions

    End Sub

    Public Function lineSeparator() As ArrayList

        For i = LBound(instruccions) To UBound(instruccions)
            lexical = RegisterToken(instruccions(i), i)
        Next i

        Return tokenList
    End Function

    Public Function RegisterToken(line As String, row As Integer) As Dictionary(Of String, ArrayList)
        Dim result As New Dictionary(Of String, ArrayList)
        Dim character As Char
        Dim state As Integer = 0
        Dim lexeme As String
        Dim column As Integer
        For i = 1 To Len(line)
            character = Mid(line, i, 1)
            Select Case state
                Case 0
                    Select Case character
                        Case "\r"
                        Case " "
                        Case vbTab
                        Case "\b"
                        Case "\f"
                        Case "["
                            column = column + 1
                            Dim token As New Token(character, "Corchete Abierto", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "]"
                            column = column + 1
                            Dim token As New Token(character, "Corchete Cerrado", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "="
                            column = column + 1
                            Dim token As New Token(character, "Signo Igual", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "{"
                            column = column + 1
                            Dim token As New Token(character, "Llave Abierta", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "}"
                            column = column + 1
                            Dim token As New Token(character, "Llave Cerrada", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "+"
                            column = column + 1
                            Dim token As New Token(character, "Visibilidad", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "-"
                            column = column + 1
                            Dim token As New Token(character, "Visibilidad", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "#"
                            column = column + 1
                            Dim token As New Token(character, "Visibilidad", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case ":"
                            column = column + 1
                            Dim token As New Token(character, "Dos puntos", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case ";"
                            column = column + 1
                            Dim token As New Token(character, "Punto y coma", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case "("
                            column = column + 1
                            Dim token As New Token(character, "Parentesis Abierto", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case ")"
                            column = column + 1
                            Dim token As New Token(character, "Parentesis Cerrado", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case """"
                            column = column + 1
                            Dim token As New Token(character, "Comillas", column, row + 1)
                            tokenList.Add(token)
                            state = 4
                        Case ","
                            column = column + 1
                            Dim token As New Token(character, "Coma", column, row + 1)
                            tokenList.Add(token)
                            state = 0
                        Case Else
                            If Char.IsLetter(character) Then
                                lexeme = character
                                state = 2
                                column = column + 1
                            ElseIf Char.IsDigit(character) Then
                                column = column + 1
                                Dim token As New Token(character, "Numero", column, row + 1)
                                tokenList.Add(token)
                                state = 0
                            Else
                                column = column + 1
                                Dim token As New Token(character, column, row + 1, "Caracter desconocido", "Lexico", "Desconocido")
                                errorList.Add(token)
                                state = 0
                            End If
                    End Select
                Case 2
                    Select Case character
                        Case "_"
                            lexeme = lexeme + character
                            state = 3
                        Case Else
                            If Char.IsLetter(character) Then
                                lexeme = lexeme + character
                                state = 2
                            ElseIf Char.IsDigit(character) Then
                                lexeme = lexeme + character
                                state = 3
                            Else
                                Dim type As String = identifyType(lexeme.ToLower())
                                Dim token As New Token(lexeme.ToLower(), type, column, row + 1)
                                tokenList.Add(token)
                                state = 0
                                lexeme = ""
                                i = i - 1
                            End If

                    End Select
                Case 3
                    Select Case character
                        Case Else
                            If Char.IsLetter(character) Then
                                lexeme = lexeme + character
                                state = 3
                            ElseIf Char.IsDigit(character) Then
                                lexeme = lexeme + character
                                state = 3
                            Else
                                Dim type As String = identifyType(lexeme.ToLower())
                                Dim token As New Token(lexeme.ToLower(), type, column, row + 1)
                                tokenList.Add(token)
                                state = 0
                                lexeme = ""
                                i = i - 1
                            End If
                    End Select
                Case 4
                    Select Case character
                        Case """"
                            Dim type As String = "Comentario"
                            Dim token As New Token(lexeme.ToLower(), type, column, row + 1)
                            tokenList.Add(token)
                            lexeme = ""
                            column = column + 1
                            Dim token2 As New Token(character, "Comillas", column, row + 1)
                            tokenList.Add(token2)
                            state = 0
                        Case Else
                            lexeme = lexeme + character
                            state = 4
                    End Select
            End Select
        Next i
        result.Add("tokens", tokenList)
        If errorList.Count > 0 Then
            result.Add("errors", errorList)
        End If
        Return result
    End Function

    Public Function identifyType(lexeme As String) As String
        If lexeme.Equals("clase") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("nombre") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("atributos") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("metodos") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("asociacion") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("agregacion") Then
            Return "Tipo de Asociacion"
        End If
        If lexeme.Equals("composicion") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("herencia") Then
            Return "Tipo de Asociacion"
        End If
        If lexeme.Equals("asociacionsimple") Then
            Return "Tipo de Asociacion"
        End If
        If lexeme.Equals("color") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("comentario") Then
            Return "Palabra reservada"
        End If
        If lexeme.Equals("texto") Then
            Return "Palabra reservada"
        End If
        Return "Identificador"
    End Function

End Class
