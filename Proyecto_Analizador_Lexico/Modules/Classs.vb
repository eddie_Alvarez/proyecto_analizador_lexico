﻿Public Class Classs

#Region "Declarations"
    Private name As String
    Private color(3) As Integer
    Private method As ArrayList
    Private attribute As New ArrayList
#End Region

#Region "Properties"

    Public Property names As String
        Get
            Return Me.name
        End Get
        Set(value As String)
            Me.name = value
        End Set
    End Property

    Public Property colors As Integer()
        Get
            Return Me.color
        End Get
        Set(value() As Integer)
            Me.color = value
        End Set
    End Property

    Public Property methods As ArrayList
        Get
            Return Me.method
        End Get
        Set(value As ArrayList)
            Me.method = value
        End Set
    End Property

    Public Property attributes As ArrayList
        Get
            Return Me.attribute
        End Get
        Set(value As ArrayList)
            Me.attribute = value
        End Set
    End Property

#End Region

#Region "Constructors"
    Public Sub New(name As String, color As Integer(), method As ArrayList, attribute As ArrayList)
        Me.name = name
        Me.color = color
        Me.method = method
        Me.attribute = method
    End Sub
#End Region

End Class
