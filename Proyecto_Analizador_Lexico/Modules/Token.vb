﻿Public Class Token

#Region "Declarations"
    Private lexeme As String
    Private type As String
    Private column As Integer
    Private line As Integer
    Private descriptionError As String
    Private typeError As String
#End Region

#Region "Properties"
    Public Property Lexemes As String
        Get
            Return Me.lexeme
        End Get
        Set(value As String)
            Me.lexeme = value
        End Set
    End Property

    Public Property descritptionErrors As String
        Get
            Return Me.descriptionError
        End Get
        Set(value As String)
            Me.descriptionError = value
        End Set
    End Property

    Public Property typeErrors As String
        Get
            Return Me.typeError
        End Get
        Set(value As String)
            Me.typeError = value
        End Set
    End Property

    Public Property types As String
        Get
            Return Me.type
        End Get
        Set(value As String)
            Me.type = value
        End Set
    End Property

    Public Property columns As Integer
        Get
            Return Me.column
        End Get
        Set(value As Integer)
            Me.column = value
        End Set
    End Property

    Public Property lines As Integer
        Get
            Return Me.line
        End Get
        Set(value As Integer)
            Me.line = value
        End Set
    End Property

#End Region

#Region "Contructors"

    Public Sub New(lexeme As String, type As String, column As Integer, line As Integer)
        Me.lexeme = lexeme
        Me.type = type
        Me.column = column
        Me.line = line
    End Sub

    Public Sub New(lexeme As String, column As Integer, line As Integer, descriptionError As String, typeError As String, type As String)
        Me.lexeme = lexeme
        Me.column = column
        Me.line = line
        Me.descriptionError = descriptionError
        Me.typeError = typeError
        Me.type = type
    End Sub

#End Region

End Class
